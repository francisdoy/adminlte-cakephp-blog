<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'AdminLTE Test';
?>
<html>

<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <?= $this->Html->css('../bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
    <!-- Font Awesome -->
    <?= $this->Html->css('../bower_components/font-awesome/css/font-awesome.min.css') ?>
    <!-- Ionicons -->
    <?= $this->Html->css('../bower_components/Ionicons/css/ionicons.min.css') ?>
    <!-- Theme style -->
    <?= $this->Html->css('../dist/css/AdminLTE.min.css') ?>
    <!-- iCheck -->
    <?= $this->Html->css('../plugins/iCheck/square/blue.css') ?>

    <?= $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') ?>
    <?= $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') ?>

    <!-- Google Font -->
    <?= $this->Html->css('google-font.css') ?>

    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b>LTE
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <section class="content">
            <?= $this->fetch('content') ?>
        </section>

        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign
                in using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign
                in using
                Google+</a>
        </div>
        <!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
<!--        <a href="register.html" class="text-center">Register a new membership</a>-->
        <?= $this->Html->link('Register a new membership', ['controller' => 'Users', 'action' => 'add', 'class' => 'text-center']) ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<?= $this->Html->script('../bower_components/jquery/dist/jquery.min.js') ?>
<!-- Bootstrap 3.3.7 -->
<?= $this->Html->script('../bower_components/bootstrap/dist/js/bootstrap.min.js') ?>
<!-- iCheck -->
<?= $this->Html->script('../plugins/iCheck/icheck.min.js') ?>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>


</body>

</html>